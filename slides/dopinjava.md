# Dataoriented in Java

---slide---

## Polymorphism

* Sharing code
* Sharing type
  * Strategy pattern

---slide---

## Strategy in OO

<img style="height: 500px;" src="slides/images/strategyoo.svg">

---slide---

## Strategy in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public interface CreditCard {

  void pay();
  
}

public class MasterCard implements CreditCard {

  final String number;

  @Override
  
  public void pay() { MasterCardPayment.pay(number) }
  
}

public class VisaCard implements CreditCard {

  final String number;

  @Override
  
  public void pay() { VisaCardPayment.pay(number) }
  
}
</code>
</pre>

---slide---

## Strategy in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public void doLogic(CreditCard creditCard) {

  creditCard.pay();
  
}
</code>
</pre>

---slide---

## Strategy in DOP

<img style="height: 500px;" src="slides/images/strategydop.svg">

---slide---

## Strategy in DOP

<pre>
<code class="hljs java" style="max-height: 200%;font-size:150%;padding: 0.5em;">public interface CreditCard { }

public record MasterCard(String number) implements CreditCard { }

public record VisaCard(String number) implements CreditCard { }

</code>
</pre>

---slide---

## Strategy in DOP

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public void doLogic(CreditCard creditCard){

  switch (creditCard){
  
  
    case MasterCard mc -> MasterCardPayment.pay(mc);
	
	
    case VisaCard vc -> VisaCardPayment.pay(vc);
	
	
    default -> throw new IllegalStateException();
	
  }
  
}
</code>
</pre>

---slide---

## Introducing sealed classes

<pre>
<code class="hljs java" style="max-height: 200%;font-size:150%;padding: 0.5em;">public sealed interface CreditCard permits VisaCard, MasterCard { }

public record MasterCard(String number) implements CreditCard { }

public record VisaCard(String number) implements CreditCard { }

</code>
</pre>

---slide---

## Introducing sealed classes

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public void doLogic(CreditCard creditCard){

  switch (creditCard){
  
  
    case MasterCard mc -> MasterCardPayment.pay(mc);
	
	
    case VisaCard vc -> VisaCardPayment.pay(vc);
	
  }
  
}
</code>
</pre>

---slide---

## Branch operation

<img style="height: 500px;" src="slides/images/branchoperation.svg">

---slide---

## Gamechanger

* New way of using polymorphism
* Can help with
  * Thinking in state changes
  * Error handling
  * Immutable lists