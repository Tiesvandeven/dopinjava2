# Conclusion

* Most applications are data streams
* Data can be denormalized for performance gains
* Separating state from functionality can offer better scaling
* We have a gamechanger with compile time safe branch operations

---slide---

>> "Don't be a Functional programmer, don't be an Object Oriented programmer, Don't be a Data Oriented Programmer, be a _better_ programmer"

- Brian Goetz + Ties van de Ven

---slide---

## Keep in touch

<img style="float: right;" src="slides/images/ties.jpg">

* ties_ven @X (Or twitter)
* www.tiesvandeven.nl
* codingcoach.io
* Or talk to me in real life
* Sheets: https://tiesvandeven.gitlab.io/dopinjava2/