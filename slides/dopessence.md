# Dataoriented mindset

---slide---

## Its all about the data

* Most applications are data streams
* Data streams is just data transformation

---slide---

<img style="height: 300px;" src="slides/images/sourcesink.svg">

---slide---

<img style="height: 500px;" src="slides/images/flow.svg">

---slide---

<img style="height: 400px;" src="slides/images/flowexample.svg">

---slide---

## Thats a lot of types

Json &rarr; HashMap &rarr; User &rarr; Entity &rarr; Response &rarr; HashMap &rarr; Json

---slide---

## Downsides of types

* Types don't naturally map to other types
  * Custom mappers
  * Reflection
* HashMap does
  * .map()

---slide---

## Data as Data

Json &rarr; HashMap &rarr; HashMap &rarr; HashMap &rarr; HashMap &rarr; Json

---slide---

## So should we stop using types?

* Benefits
  * Compile time checks
  * Less tests
* What can we learn
  * Different ways to model data
  * Normalized vs denormalized data

---slide---

## Normalized vs denormalized

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em; float:left">public class Person {

  private int age;
  
  private String name;
  
}


List&lt;Person> persons;
</code>
</pre>


<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em; float:right"> public class Persons {

  private List&lt;Integer> ages;
  
  private List&lt;String> names;
  
}
</code>
</pre>
