# Java's new paradigm

---slide---

# WHOAMI

<img style="float: right; height: 500px;" src="slides/images/ties.jpg">

* Ties van de Ven
* Software Quality Expert
* Java/Kotlin/Scala
* Sharing knowledge
* Software engineering fundamentals / concepts

---slide---


<img style="height: 500px;" src="slides/images/dop.png">


https://www.infoq.com/articles/data-oriented-programming-java/

---slide---

# New language features

* Records
* Sealed classes
* Pattern matching (with instanceof)

---slide---

# What this talk is _NOT_ about

* Language features
* A "better" way of programming

---slide---

>> "Don't be a Functional programmer, don't be an Object Oriented programmer, be a _better_ programmer"

- Brian Goetz

---slide---

# Agenda

* Quick recap
* Dataoriented mindset
* Dataoriented architecture
* Dataoriented in Java