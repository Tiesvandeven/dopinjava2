# Object Oriented 
vs
# Functional Programming
vs
# Data Oriented Programming

---slide---

## What is OO

* Everything is an object
* Encapsulation
* Boundaries
* Tell-Don't-Ask

---slide---

## Ask

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;" data-line-numbers="1-8|9-16">class Car {

  private int currentFuel; // getter + setter
	
  private int maxFuel; // getter
	
}

public void addFuel(Car car, int amount){

  if(car.getCurrentFuel() + amount < car.getMaxFuel()){
	
    car.setCurrentFuel(car.getCurrentFuel() + amount);
		
  }
}
</code>
</pre>

---slide---

## Tell

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">class Car {

  private int currentFuel;
	
  private int maxFuel;
  

  public void addFuel(int amount){
	
    if(currentFuel + amount < maxFuel) {
		
      currentFuel = currentFuel + amount;
			
    }
		
  }
	
}
</code>
</pre>

---slide---

## What is OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;" data-line-numbers="1-8|7">class Car {

  private int currentFuel;
	
  private int maxFuel;
		
  public void doBusinessLogic() {}
  
}
</code>
</pre>

---slide---

## What is FP

* Everything is a function
* Composition

<br/>

<img style="height: 400px;" src="slides/images/domino.jpg">

---slide---

## What is DOP

* Everything is data

---slide---

## What is DOP

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;" data-line-numbers="1-7|3,5">public record Car(

  int currentFuel,
  
  int maxFuel
  
){}
</code>
</pre>
