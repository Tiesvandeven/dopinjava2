# Dataoriented architecture

---slide---

## OO vs DOP

* OO combines state with functionality
  * Use cases live with data
    * Tell-Don't-Ask
    * Easy to see operations on data
* DOP separates them
  * Use cases live in separate components
    * Cleaner object graph
    * Can scale better

---slide---

## Implementing save in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public class Person {
		
  public void save(){
  
		//
		
  }
}
</code>
</pre>

---slide---

## Implementing save in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public class Person {

  private final DataAccessHelper dataAccessHelper;
  

  public Person(DataAccessHelper dataAccessHelper){
  
    this.dataAccessHelper = dataAccessHelper;
	
  }

  public void save(){
  
    dataAccessHelper.save(this);
	
  }
  
}
</code>
</pre>

---slide---

## Implementing save in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public class Person {

  public void save(){
  
    DataAccessHelper.getInstance().save(this);
	
  }
  
}
</code>
</pre>

---slide---

## Implementing save in OO

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public class Person {

  public void save(DataAccessHelper dataAccessHelper){
  
    dataAccessHelper.save(this);
	
  }
  
}
</code>
</pre>

---slide---

## Dependencies in OO

<img style="height: 700px;" src="slides/images/dependenciesoo1.svg">

---slide---

## Adding a component

<img style="height: 700px;" src="slides/images/dependenciesoo2.svg">

---slide---

## Removing a component

<img style="height: 700px;" src="slides/images/dependenciesoo3.svg">

---slide---

## Applications only grow

<img style="height: 500px;" src="slides/images/data_trap_2x.png">

---slide---

## So our OO classes grow

<img style="height: 500px;" src="slides/images/soourclassesgrow.jpg">

---slide---

## The DOP way

* Create a component for every use-case
* Pass the data as arguments

---slide---

## Putting logic in components

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em;">public class PersonPersisterDatabase {

  public void persist(Person person){
  
    dataAccessHelper.save(person);
	
  }
  
}
</code>
</pre>

---slide---

## Dependencies in DOP

<img style="height: 700px;" src="slides/images/dependenciesdop1.svg">

---slide---

## Adding a component

<img style="height: 700px;" src="slides/images/dependenciesdop2.svg">

---slide---

## Removing a component

<img style="height: 700px;" src="slides/images/dependenciesdop3.svg">

---slide---

## But this looks familiar...

---slide---

## Should you combine state and functionality?

* Be careful with dependencies
* Be careful with business logic
* Be pragmatic not dogmatic
